#!/usr/bin/env python
"""
%prog [options] username email

Add a new user account to the MLF2 CouchDB
"""
import couchdb
import json
import os
import hashlib
import getpass
from optparse import OptionParser

def hash_password(password, salt=None):
    salt = (salt or os.urandom(16)).encode('hex')
    h = hashlib.sha1()
    h.update(password)
    h.update(salt)
    return h.hexdigest(), salt

def store_user(db, name, password, email, roles, **opts):
    doc_id = 'org.couchdb.user:%s' % name
    doc = db.get(doc_id, {})
    doc.update({
        'type': 'user',
        'name': name,
        'roles': roles,
        'couch.app.profile': {
            'email': email
        }
    })
    for k, v in opts.items():
        doc['couch.app.profile'][k] = v
    phash, salt = hash_password(password)
    doc['password_sha'] = phash
    doc['salt'] = salt
    db[doc_id] = doc

def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(cfgfile=os.path.expanduser('~/.mlf2dbrc'), password=None,
                        roles='')
    parser.add_option('-c', '--cfgfile',
                      type='string',
                      dest='cfgfile',
                      metavar='FILE',
                      help='override default configuration file ~/.mlf2dbrc')
    parser.add_option('-p', '--password',
                      type='string',
                      dest='password',
                      help='enter a password for the new account')
    parser.add_option('-r', '--roles',
                      type='string',
                      dest='roles',
                      metavar='ROLE1[,ROLE2,...]',
                      help='specify one or more roles for the user')

    opts, args = parser.parse_args()
    if len(args) < 2:
        parser.error('Missing arguments')

    name, email = args[0:2]

    cfg = json.load(open(opts.cfgfile, 'rb'))
    admin_pass = getpass.getpass(prompt='Enter admin password:')
    url = 'http://admin:%s@%s:%d' % (admin_pass,
                                     cfg['host'],
                                     cfg['port'])
    if not opts.password:
        opts.password = getpass.getpass('Enter user password:')

    server = couchdb.client.Server(url)
    store_user(server['_users'], name, opts.password, email, opts.roles.split(','))

if __name__ == '__main__':
    main()

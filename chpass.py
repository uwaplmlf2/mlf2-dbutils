#!/usr/bin/env python
#
"""
%prog [options] username

Change the password for an MLF2 CouchDB user account
"""
import couchdb
import json
import os
import sys
import hashlib
import getpass
from optparse import OptionParser

def hash_password(password, salt=None):
    salt = (salt or os.urandom(16)).encode('hex')
    h = hashlib.sha1()
    h.update(password)
    h.update(salt)
    return h.hexdigest(), salt

def update_password(db, name, password):
    doc_id = 'org.couchdb.user:%s' % name
    doc = db.get(doc_id)
    if not doc:
        raise KeyError
    phash, salt = hash_password(password)
    doc['password_sha'] = phash
    doc['salt'] = salt
    db[doc_id] = doc

def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(cfgfile=os.path.expanduser('~/.mlf2dbrc'), password=None)
    parser.add_option('-c', '--cfgfile',
                      type='string',
                      dest='cfgfile',
                      metavar='FILE',
                      help='override default configuration file ~/.mlf2dbrc')
    parser.add_option('-p', '--password',
                      type='string',
                      dest='password',
                      help='enter a password for the new account')

    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing user name')

    name = args[0]

    cfg = json.load(open(opts.cfgfile, 'rb'))
    if not opts.password:
        opts.password = getpass.getpass('Enter old password:')
    url = 'http://%s:%s@%s:%d' % (name, opts.password,
                                  cfg['host'],
                                  cfg['port'])
    server = couchdb.client.Server(url)
    new_password = getpass.getpass('Enter new password:')
    if(new_password != getpass.getpass('Repeat new password:')):
        sys.stderr.write('Passwords do not match. Exiting\n')
        sys.exit(1)

    update_password(server['_users'], name, new_password)

if __name__ == '__main__':
    main()


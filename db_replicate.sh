#!/bin/bash
#
# Replicate a remote CouchDB database to the local host
#
: ${SERVER="mlf2data.apl.uw.edu:5984"}

dbname="$1"
if [[ -z "$dbname" ]]; then
    echo "Usage: $(basename $0) dbname" 1>&2
    exit 1
fi

CREDENTIALS=$(cat $HOME/.dbcredentials 2>/dev/null)
opts="-s"
[[ -n "$CREDENTIALS" ]] && opts="$opts --user $CREDENTIALS"

doc_id="replicate_${dbname}"
resp=$(curl $opts -X GET "http://localhost:5984/_replicator/$doc_id")

if echo "$resp" | grep -q triggered; then
    # Replication already in progress
    exit 0
fi

if echo "$resp" | grep -q error; then
    curl $opts -X POST "http://localhost:5984/_replicator" \
      -H 'Content-type: application/json' \
      -d @- <<EOF
{
    "_id": "$doc_id",
    "source": "http://$SERVER/$dbname",
    "target": "$dbname",
    "connection_timeout": 60000,
    "retries_per_request": 20,
    "http_connections": 8,
    "continuous": true,
    "user_ctx": {"name": "admin", "roles": ["_admin", "message_send", "mlf2_admin", "data_source"]}
}
EOF
else
    # Get the document revision
    rev=$(echo $resp | sed -e 's/.*"_rev":"\([^"]*\)".*/\1/')
    # Update the document
    curl $opts -X POST "http://localhost:5984/_replicator" \
      -H 'Content-type: application/json' \
      -d @- <<EOF
{
    "_id": "$doc_id",
    "_rev": "$rev",
    "source": "http://$SERVER/$dbname",
    "target": "$dbname",
    "connection_timeout": 60000,
    "retries_per_request": 20,
    "http_connections": 8,
    "continuous": true,
    "user_ctx": {"name": "admin", "roles": ["_admin", "message_send", "mlf2_admin", "data_source"]}
}
EOF
fi

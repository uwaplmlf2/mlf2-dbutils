#!/usr/bin/env python
#
# Script to start the backup EC2 image for the MLF2 server. Requires that the access
# keys used by the boto package (https://github.com/boto/boto) are stored in the
# configuration file ~/.boto.
#
# The access keys can be downloaded from the AWS Account Management Site. See the
# URL below for instructions:
#
#  http://aws.amazon.com/articles/3998
#
import boto
import boto.ec2
import os
import time
import sys

# Amazon Machine ID for our backup
AMI = 'ami-01e5bf44'
# Deployment region
REGION = 'us-west-1b'
# EC2 instance type
INSTANCE_TYPE = 't1.micro'
# Security Group Name
SECURITY_GROUP = 'mlf2server'
# Key-pair name
KEY_PAIR = 'awsmlf2'
# Elastic IP to assign
IP_ADDR = '50.18.188.61'

# The following script will be passed as user_data and run at boot
# time to copy the SSH public key from the ubuntu account to the
# mq account
USER_SCRIPT = """#!/bin/bash
cp /home/ubuntu/.ssh/authorized_keys /home/mq/.ssh/
chown mq:mq /home/mq/.ssh/authorized_keys
chmod 600 /home/mq/.ssh/authorized_keys
"""

def msg(text):
    sys.stderr.write(text)
    sys.stderr.flush()

ec2 = boto.ec2.connect_to_region(REGION)

name = None
ans = raw_input('Generate a new SSH key-pair [yN]? ')
if ans in ('y', 'Y'):
    name = raw_input('Enter key-pair name (no spaces): ')
    if name:
        name = name.replace(' ', '_')
        KEY_PAIR = name
        kp = ec2.create_key_pair(KEY_PAIR)
        kp.save(os.path.expanduser('~/.ssh'))

try:
    reservation = ec2.run_instances(image_id=AMI,
                                    key_name=KEY_PAIR,
                                    instance_type=INSTANCE_TYPE,
                                    security_groups=[SECURITY_GROUP],
                                    user_data=USER_SCRIPT)
except boto.exception.EC2ResponseError as e:
    # If we created a new key-pair, delete it
    if name:
        ec2.delete_key_pair(name)
    sys.stderr.write(str(e) + '\n')
    sys.exit(1)

instance = None
for r in ec2.get_all_instances():
    if r.id == reservation.id:
        instance = r.instances[0]

msg('Waiting for instance to start ')
instance.update()
while instance.state != 'running':
    time.sleep(5)
    msg('.')
    instance.update()
msg('done\n')

host = instance.public_dns_name

print 'Instance: ', instance.id
print 'State: ', instance.state
print 'Type: ', instance.instance_type
print 'Host: %s (%s)' % (host, instance.ip_address)

ans = raw_input('Would you like to attach the Elastic IP address [y/N]? ')
if ans in ('y', 'Y'):
    ec2.associate_address(instance.id, IP_ADDR)
    host = IP_ADDR
    print 'IP address set to %s' % IP_ADDR

print 'SSH Command to access host: slogin -i ~/.ssh/%s.pem ubuntu@%s' % (KEY_PAIR,
                                                                         host)

#!/usr/bin/env python
#
# Create the configuration file for the dbutil scripts.
#
import json
import os
import getpass
import sys

DEFCONFIG = {
    "host": "mlf2data.apl.uw.edu",
    "port": 5984,
    "mlf2db": "mlf2db",
    "rudicsdb": "rudics"
}

def abort(msg):
    sys.stderr.write(msg + '\n')
    sys.exit(1)

def main():
    d = DEFCONFIG
    name = raw_input('Enter database username: ')
    if not name:
        abort('No username entered')
    password = getpass.getpass('Enter database password: ')
    dirname = raw_input('Enter float-marker directory path [~/float-markers]: ')
    if dirname:
        dirname = os.path.expanduser(dirname)
    else:
        dirname = os.path.expanduser('~/float-markers')
    d['username'] = name
    d['password'] = password
    d['markers'] = dirname
    ans = raw_input('Write config file [yN]? ')
    if ans in ('y', 'Y'):
        cfgfile = os.path.expanduser('~/.mlf2dbrc')
        outf = open(cfgfile, 'wb')
        json.dump(d, outf, indent=4)
        outf.close()
        os.chmod(cfgfile, 0600)
        print 'New config file written'

if __name__ == '__main__':
    main()



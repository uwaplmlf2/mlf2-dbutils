#!/usr/bin/env python
#
"""
%prog [options]

Add a new MLF2 deployment (mission) to the database.
"""
import couchdb
import os
import re
import json
from datetime import datetime, timedelta
from optparse import OptionParser
import sys
import pprint

def format_time(t):
    return t.strftime('%Y-%m-%dT%H:%M:%S+00:00')

def store_deployment(db, tag, desc):
    doc_id = 'apl.uw.mlf2:' + tag
    doc = db.get(doc_id, {})
    doc.update({'type': 'deployment'})
    doc.update(desc)
    db[doc_id] = doc
    return doc_id

def ask(prompt, default=''):
    r = raw_input('%s [%s]: ' % (prompt, default))
    return r or default

def ask_time(prompt, default=None):
    sys.stdout.write(prompt + '\n')
    now = default or datetime.utcnow()
    d = ask('    Date YYYY-mm-dd', default=now.strftime('%Y-%m-%d'))
    t = ask('    Time HH:MM', default='00:00')
    return datetime.strptime('%s %s' % (d, t), '%Y-%m-%d %H:%M')

def gather_info():
    """
    Prompt the user for deployment information
    """
    desc = dict()
    tag = ask('Enter deployment ID (no spaces)')
    if not re.match(r'[a-zA-Z][a-zA-Z0-9]+$', tag):
        raise ValueError, 'Invalid ID'
    desc['name'] = ask('Enter deployment name', '%s Deployment' % tag.upper())
    start = ask_time('Enter start date/time (UTC)')
    desc['start'] = format_time(start)
    stop = ask_time('Enter end date/time (UTC)', default=start+timedelta(days=1))
    desc['stop'] = format_time(stop)
    floats = ask('Enter a space-separated list of float IDs')
    desc['floats'] = [int(f) for f in floats.strip().split()]
    return tag, desc

def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(cfgfile=os.path.expanduser('~/.mlf2dbrc'))
    parser.add_option('-c', '--cfgfile',
                      type='string',
                      dest='cfgfile',
                      metavar='FILE',
                      help='override default configuration file ~/.mlf2dbrc')
    opts, args = parser.parse_args()

    cfg = json.load(open(opts.cfgfile, 'rb'))
    if 'username' in cfg:
        url = 'http://%s:%s@%s:%d' % (cfg['username'],
                                      cfg['password'],
                                      cfg['host'],
                                      cfg['port'])
    else:
        url = 'http://%s:%d' % (cfg['host'], cfg['port'])

    server = couchdb.client.Server(url)
    tag, doc = gather_info()
    pprint.pprint(doc, indent=4)
    ans = raw_input('Store new document? [yN] ')
    if ans in ('y', 'Y'):
        try:
            doc_id = store_deployment(server[cfg['mlf2db']], tag, doc)
            print 'Added new document: %s' % doc_id
        except Exception as e:
            print str(e)

if __name__ == '__main__':
    main()


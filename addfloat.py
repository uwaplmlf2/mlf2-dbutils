#!/usr/bin/env python
#
"""
%prog [options] floatid IMEI

Add a new float entry to the MLF2 and RUDICS databases.
"""
import couchdb
import os
import json
from optparse import OptionParser


def store_float(db, floatid, imei, marker_dir, group=None):
    """
    Add a float document to the database. This will overwrite any existing
    document with the same ID.

    :param db: CouchDB database object
    :param floatid: float ID number
    :param imei: float Iridium serial number (International Mobile Equipment
    Identity)
    :param marker_dir: directory containing float marker images
    :return: new document ID

    """
    doc_id = 'apl.uw.mlf2:%d' % floatid
    doc = db.get(doc_id, {})
    doc.update({'type': 'active',
                'imei': imei,
                'floatid': floatid})

    if group is not None:
        doc['group'] = group
    db[doc_id] = doc
    if os.path.isdir(marker_dir):
        f = open(os.path.join(marker_dir, 'float%d.png' % floatid), 'rb')
        db.put_attachment(db[doc_id], f, 'marker.png',
                          content_type='image/png')
    return doc_id


def store_rudics(db, floatid, imei):
    """
    Add a new entry to the RUDICS database for an MLF2 float.

    :param db: CouchDB database object
    :param floatid: float ID number
    :param imei: float Iridium serial number
    """
    doc_id = imei
    doc = db.get(doc_id, {})
    doc.update({'type': 'local',
                'imei': imei,
                'name': 'mlf2-%d' % floatid})
    db[doc_id] = doc
    return doc_id


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(cfgfile=os.path.expanduser('~/.mlf2dbrc'),
                        group=None,
                        markerdir=None)
    parser.add_option('-c', '--cfgfile',
                      type='string',
                      dest='cfgfile',
                      metavar='FILE',
                      help='override default configuration file ~/.mlf2dbrc')
    parser.add_option('-m', '--markers',
                      type='string',
                      dest='markerdir',
                      metavar='DIR',
                      help='directory of float marker images')
    parser.add_option('-g', '--group',
                      type='string',
                      dest='group',
                      help='group that this float belongs to')

    opts, args = parser.parse_args()
    if len(args) < 2:
        parser.error('Missing arguments')

    floatid = int(args[0])
    imei = args[1]

    cfg = json.load(open(opts.cfgfile, 'rb'))
    # Expand env variables in marker path
    if 'markers' in cfg:
        cfg['markers'] = os.path.expandvars(cfg['markers'])
    else:
        cfg['markers'] = opts.markerdir

    if 'username' in cfg:
        url = 'http://%s:%s@%s:%d' % (cfg['username'],
                                      cfg['password'],
                                      cfg['host'],
                                      cfg['port'])
    else:
        url = 'http://%s:%d' % (cfg['host'], cfg['port'])

    server = couchdb.client.Server(url)
    doc_id = store_float(server[cfg['mlf2db']], floatid, imei,
                         cfg['markers'], group=opts.group)
    print 'New mlf2db document: %s' % doc_id
    doc_id = store_rudics(server[cfg['rudicsdb']], floatid, imei)
    print 'New rudics document: %s' % doc_id


if __name__ == '__main__':
    main()
